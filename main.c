#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node
{
    char name[50];
    int jmbg;
    double salary;
    struct Node * next;
} Node;

Node * addNode(Node * list, Node * node);
void inputData(Node * node);
Node * getNode(Node * list, int jmbg);
void removeNode(Node * list, int jmbg);
void printNodes(Node * list);

int main()
{
    int command, jmbg;
    Node * list = 0;
    Node * node = 0;

    while(1)
    {
        printf("\nBAZA ZAPOSLENIH\n");
        printf("(1) Dodajte zaposlenog\n");
        printf("(2) Izmenite podatke o zaposlenom\n");
        printf("(3) Prikaz svih zaposlenih\n");
        printf("(4) Izlaz\n");
        scanf("%d", &command);

        switch(command)
        {
        case 1 :
            node = malloc(sizeof(Node));
            inputData(node);
            node->next = 0;
            list = addNode(list, node);
            break;

        case 2 :
            printf("Unesite jmbg zaposlenog cije podatke zelite da izmenite\n");
            scanf("%d", &jmbg);
            node = getNode(list, jmbg);
            if(!node)
                printf("Zaposleni sa tim jmbg ne postoji\n");
            else
                inputData(node);
            break;

        case 3 :
            printNodes(list);
            break;

        case 4 :
            exit(EXIT_SUCCESS);
            break;

        default :
            printf("Invalidna komanda\n");
        }
    }

    return 0;
}

Node * addNode(Node * list, Node * node)
{
    if(list == 0)
    {
        list = node;
        list->next = 0;
        return list;
    }

    Node * curr = list;
    for(; curr->next; curr = curr->next);
    curr->next = node;
    node->next = 0;

    return list;
}

void inputData(Node * node)
{
    char name[50];
    int jmbg;
    double salary;
    printf("Unesite ime novog zaposlenog\n");
    scanf("%s", name);
    printf("Unesite jmbg novog zaposlenog\n");
    scanf("%d", &jmbg);
    printf("Unesite platu novog zaposlenog\n");
    scanf("%lf", &salary);
    strcpy(node->name, name);
    node->jmbg = jmbg;
    node->salary = salary;
}

Node * getNode(Node * list, int jmbg)
{
    Node * curr = list;

    while(curr && curr->jmbg != jmbg)
        curr = curr->next;

    return curr;
}

void printNodes(Node * list)
{
    printf("\n\nSVI ZAPOSLENI\n\n");
    for(Node * curr = list; curr; curr = curr->next)
        printf("Ime: %s\nJMBG: %d\nPlata: %lf\n\n", curr->name, curr->jmbg, curr->salary);
}



Node * removeNode(Node * list, int jmbg)
{
    return null;
}
